# Sort Files By Date

## Basic Usage

In Windows, you can simply copy the py file into the folder you wish to organize and run it (assuming you have [python installed](https://www.python.org/downloads/)). Otherwise from the appropriate terminal, call `sortfile.py` with your desired arguments (I recomment at least using -p, otherwise the install file will be sorted).

## Details

A simple script to move any files in a folder into a date folder, formatted YYYY-MM-DD.

The script accepts three arguments, none of which are required.

| Argument | Example | Description |
| :------: | ------- | ----------- |
| -p | `py sortfile.py -p "D:\Desktop\Myfiles"` | Define the folder path in which to operate. If not defined, the script will sort the folder in which the script resides. |
| -e | `py sortfile.py -e ".jpg, .png"` | Declare any file formats that you wish to skip sorting. Listed items must be comma seperated, spacing will be ignored. ideally the `.` should be included, results may be slightly less predicable if left out. Also whole file names can be included. |
| -i | `py sortfile.py -i "CHANGELOG, LICENCE"` | Force the script to include the listed files |
| -v | `py sortfile.py -v` | Force the script to show you each file it has successfully moved. This is not required but may be helpful in seeing the progress of the script in action. It may however slow the execution of the script. Leave this off if speed is required. |

## Note

In general this script is used for sorting photos or documents. The following files are excluded from being sorted:

- `sortfile.py`
- `README.md`
- `LICENSE`
- `.gitignore`
- `CHANGELOG`

If you wish to include any of the above files, please make the `-i` argument when running the script
