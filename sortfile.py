#!/usr/bin/python3

## Author   Gareth Palmer
##          https://gitlab.com/projector22
##          https://github.com/projector22
## Repo     https://gitlab.com/projector22/sort-files-by-date
## Licence  MIT
## Version  1.1


def in_exclude_list(name, exclude_these = None, include_these = None):
    """
    Check if any the file being checked is in the exclude list or excluded by argument.

    @param - string - name - The name of the file which is being examined

    @param - list - exclude_these - A list of user defined exclusions. Default: None.

    @param - list - include_these - A list of user defined inclusions. Default: None.

    @return boolean
    """
    exclude = ['sortfile.py', 'README.md', 'LICENSE', '.gitignore', 'CHANGELOG']
    if exclude_these != None:
        exclude += exclude_these
    if include_these != None:
        for item in include_these:
            if item.strip() in exclude:
                exclude.remove(item.strip())
    for item in exclude:
        if item.strip() in name:
            return True
    return False


def fix_numbering(number):
    """
    Fix numbering by adding a leading 0 to numbers less than 10, send by file_date.(day/month)

    @param - number - The number being examined

    @return - the "fixed" number with a leading 0 as needed
    """
    if number < 10:
        return '0' + str(number)
    else:
        return number


from os import listdir, path, makedirs, rename
from os.path import isfile, join, isdir, getmtime
import time, datetime, argparse, platform

ap = argparse.ArgumentParser()
ap.add_argument("-v", "--verbose", action='store_true', required=False, help="Show each file as it is parsed")
ap.add_argument("-p", "--path", required=False, help="Define the path which the program should operate on")
ap.add_argument("-e", "--exclude", required=False, help="Name some file extensions to exclude")
ap.add_argument("-i", "--include", required=False, help="Name some files to include")
arg = vars(ap.parse_args())

if arg['path'] != None:
    mypath = arg['path']
else:
    mypath = path.dirname(path.abspath(__file__))

if arg['exclude'] != None:
    exclude = arg['exclude'].split(',')
else:
    exclude = None

if arg['include'] != None:
    include = arg['include'].split(',')
else:
    include = None

if platform.system() == 'Windows':
    sep = '\\'
else:
    sep = '/'

file_list = [f for f in listdir(mypath) if isfile(join(mypath, f))]
count = 0

for item in file_list:
    if in_exclude_list(item, exclude, include):
        continue
    file_path = mypath + sep + item
    file_date = time.localtime(getmtime(file_path))
    folder_name = str(file_date.tm_year) + '-' + str(fix_numbering(file_date.tm_mon)) + '-' + str(fix_numbering(file_date.tm_mday))
    target_folder = mypath + sep + folder_name
    if not isdir(target_folder):
        makedirs(target_folder)
    rename(file_path, target_folder + sep + item)
    count = count + 1
    if arg['verbose'] == True:
        print(item, 'moved')

if arg['verbose'] == True:
    print(count, 'items sorted')
